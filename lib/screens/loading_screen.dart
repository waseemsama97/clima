import 'package:clima/screens/location_screen.dart';
import 'package:flutter/material.dart';
import 'package:clima/services/location.dart';
import 'package:clima/services/networking.dart';
import 'package:clima/utilities/constants.dart';
//import 'location_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  static Location Loc;
  NetworkHelper NH;
  @override
  void initState() {
    super.initState();
    Loc = new Location();
    getlocationData();
  }

  void getlocationData() async {
    await Loc.GetCurrentLocation();
    NH = new NetworkHelper(
        URL:
            'https://api.openweathermap.org/data/2.5/weather?lat=${Loc.Lat}&lon=${Loc.Lon}&appid=$OWAPI&units=metric');
    var data = await NH.getData();
    print(data);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LocationScreen(
        WeatherData: data,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitFadingCircle(
          color: Colors.white,
          size: 50.0,
        ),
      ),
    );
  }
}
