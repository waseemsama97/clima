import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class NetworkHelper {
  Location Loc;
  final String URL;

  NetworkHelper({@required this.URL});

  Future getData() async {
    var respones = await http.get(Uri.parse(URL));
    if (respones.statusCode == 200) {
      //print(respones.body);
      return jsonDecode(respones.body);
    } else {
      //print(respones.statusCode);
      return respones.statusCode;
    }
  }
}
