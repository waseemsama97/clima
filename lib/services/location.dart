import 'package:geolocator/geolocator.dart';

class Location {
  double Lat, Lon;

  Future<void> GetCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
      Lat = position.latitude;
      Lon = position.longitude;
    } catch (e) {
      print(e);
    }
  }
  
}
